﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class MotorcycleController : MonoBehaviour
{
    public Rigidbody rb;

    public float speed, coefSpeed, maxSpeed;

    public bool isTurning;
    public float turnValue;

    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
        
        if(isTurning)
            transform.Rotate(0, turnValue, 0);
    }

    public void Move(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            speed += context.ReadValue<float>() / 2;
            if (speed > maxSpeed)
            {
                speed = maxSpeed;
            }
        }

        else if (!context.performed)
        {
            speed -= coefSpeed;
            if (speed < 20)
            {
                speed = 20f;
            }
        }
    }

    public void Turn(InputAction.CallbackContext context)
    {
        if(context.performed)
        {
            isTurning = true;

            turnValue = context.ReadValue<float>();

        }
        else if (!context.performed)
        {
            isTurning = false;
        }
    }

    /*public void Accelerate(InputAction.CallbackContext context)
    {
        if (!context.canceled)
        {
            float pressForce = context.ReadValue<float>();
            speed += pressForce;
        }
    }*/
}
