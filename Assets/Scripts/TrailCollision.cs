﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailCollision : MonoBehaviour
{
    public float timeBetweenPoints;
    public TrailRenderer trail;
    
    private List<Vector3> _pointList;
    private float _nextPointTime;
    private float _endTrailTime;

    void Start()
    {
        //Initialise the trail points list with the actual trail position
        _pointList = new List<Vector3> {transform.position};
        //Set the next point adding time
        _nextPointTime = Time.time + timeBetweenPoints;
        //Set the next point removing time by looking at the trail fade out time
        _endTrailTime = Time.time + trail.time;
    }
    
    void Update()
    {
        //If the next point adding time is reached
        if (Time.time >= _nextPointTime)
        {
            _nextPointTime = Time.time + timeBetweenPoints;
            _pointList.Add(transform.position);
        }
        //If the next point removing time is reached
        if (Time.time >= _endTrailTime)
        {
            _endTrailTime = Time.time + timeBetweenPoints;
            _pointList.RemoveAt(0);
        }
    }

    private void FixedUpdate()
    {
        //For each point in the trail point list
        for (int i = 0; i < _pointList.Count; i++)
        {
            Vector3 point1 = _pointList[i];
            Vector3 point2;
            //If it's the first point, use the actual trail position as second point
            if (i == _pointList.Count - 1)
            {
                point2 = transform.position;
            }    
            else
            {
                point2 = _pointList[i + 1];
            }
            
            Debug.DrawLine(point1, point2, Color.red);
        
            //Detect collision between trail and 
            RaycastHit hit;
            if (Physics.Linecast(point1, point2, out hit))
            {
                //TODO Collision
            }
        }
    }
}
