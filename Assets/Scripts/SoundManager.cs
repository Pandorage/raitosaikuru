﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;
    public List<SoundEffect> soundList;
    public AudioSource source;

    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        } else if (Instance != null)
        {
            Destroy(gameObject);
        }
    }

    public void PlaySound(String soundName)
    {
        var toPlay = soundList.Find(x => x.name.Equals(soundName));
        if (toPlay != null)
        {
            source.PlayOneShot(toPlay.sound);
        }
    }

    [Serializable]
    public class SoundEffect
    {
        public String name;
        public AudioClip sound;
    }
}
