﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Item class used to create the different items
//Can be used as a scriptable for a basic unusable item
[CreateAssetMenu(fileName = "Basic", menuName = "Items/Basic")]
public class Items: ScriptableObject
{
    public string displayName;
    public Color itemColor;

    //protected Player playerStats;
   
    /*
     * Called when the item is collected by a player
     * return true if the item is removed from inventory
     */
   /*
    public virtual bool OnCollect(Player player)
    {
        playerStats = player;
        return false;
    }*/

    /**
     * Called when the item is used by a player
     * return true if the item is removed from inventory
     */
    public virtual bool OnUse()
    {
        //playerStats = null;
        return false;
    }

    public virtual void OnDrop()
    {
        //playerStats = null;
    }

    /**
     * Called every frame when the item is in a player inventory
     * return true if the item is removed from inventory
     */
    public virtual bool OnUpdate()
    {
        return false;
    }
}
