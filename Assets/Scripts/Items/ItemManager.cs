﻿using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour
{
    public static ItemManager Instance;
    
    public List<Items> items;
    public GameObject fieldItem;
    public float spawnCooldown = 30f;
    public List<Transform> spawnPoints;

    private float _nextSpawnTime;

    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        } else if (Instance != null)
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        //Call the item spawning function if the time is reached, then reset it
        if (Time.time >= _nextSpawnTime)
        {
            SpawnItems();
            _nextSpawnTime = Time.time + spawnCooldown;
        }
    }

    //Destroy remaining items on map and spawn an item on each spawnpoint
    private void SpawnItems()
    {
        //For each spawnpoint
        foreach (var spawn in spawnPoints)
        {
            if (items.Count != 0)
            {
                //Choose a random item from the list
                int rnd = Random.Range(0, items.Count);
                //Make in spawn on the point
                var spawnedItem = Instantiate(fieldItem, spawn.position, spawn.rotation);
                //Remove it from list
                items.RemoveAt(rnd);
            }
        }
    }
    
    //Return an item in the list
    public void ReturnItem(Items returnedItem)
    {
       Debug.Log(returnedItem);
    }
}
