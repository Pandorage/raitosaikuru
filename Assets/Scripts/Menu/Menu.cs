﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class Menu : MonoBehaviour
{
    public GameObject optionsMenu, menu;

    public void Quit() // Quit the game
    {
        Application.Quit();
    }
    
    public void MenuStart() // Go to Start
    {
        SceneManager.LoadScene(1);
    }

    public void LoadOptions() // Open the options menu
    {
        optionsMenu.SetActive(true);
        menu.SetActive(false);
    }

    public void SetFullscreen(bool isFullscreen) // fullscreen or not
    {
        Screen.fullScreen = isFullscreen;
    }
    public void Back()  // Go back
    {
        optionsMenu.SetActive(false);
        menu.SetActive(true);
    }
}
