﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool GameIsPaused = false;
    
    public GameObject menuPause;
    public GameObject[] othersMenu;
    void Update() // Press the escape button for :
    {
        if (Input.GetKeyDown(KeyCode.Escape)) 
        {
            for (int i = 0; i < othersMenu.Length; i++)
            {
                othersMenu[i].SetActive(false);
            }
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Resume() // Resume the game
    {
        menuPause.SetActive(false);
        for (int i = 0; i < othersMenu.Length; i++)
        {
            othersMenu[i].SetActive(true);
        }
        Time.timeScale = 1f;
        GameIsPaused = false;
    }

    void Pause() // Freeze the time
    {
        menuPause.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    public void Menu() // Go to the menu
    {
        SceneManager.LoadScene(0);
        Time.timeScale = 1f;
    }
    public void Restart() // Restart the game
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1f;
    }
    public void QuitGame() // Quit the game
    {
        Application.Quit();
    }
}
